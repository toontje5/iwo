# File name: milestone5.sh
# This file counts the amount of occurences of the word "de" in the wiki page.
# Author: Victor Zwart
# Date: 11/03/2019

cat RUG_wiki_page.txt | grep "\bde\b" -io | wc -l

